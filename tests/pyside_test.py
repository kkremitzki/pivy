###
# Copyright (c) 2002-2007 Systems in Motion
#
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

###
# Pivy Coin unit test suite
#
# For detailed info on its usage and on how to write additional test cases
# read:
#   - http://pyunit.sourceforge.net/pyunit.html
#   - http://diveintopython.org/unit_testing/
#
# Invoke this script with '--help' for usage information.
#

import unittest
from PySide2 import QtCore
import shiboken2 as wrapper


class ShibokenTests(unittest.TestCase):
    """shiboken_64bit_test.py"""

    def testAdresses(self):
        from sys import platform
            q = QtCore.QObject()
            ptr = wrapper.getCppPointer(q)
            print("CppPointer to an instance of PySide.QtCore.QObject = 0x%016X" % ptr[0])

            # None of the following is expected to raise an
            # OverflowError on 64-bit systems

            # largest 32-bit address
            wrapper.wrapInstance(0xFFFFFFFF, QtCore.QObject)

            # a regular, slightly smaller 32-bit address
            wrapper.wrapInstance(0xFFFFFFF, QtCore.QObject)

            # an actual 64-bit address (> 4 GB, the first non 32-bit address)
            wrapper.wrapInstance(0x100000000, QtCore.QObject)

            # largest 64-bit address
            wrapper.wrapInstance(0xFFFFFFFFFFFFFFFF, QtCore.QObject)


if __name__ == "__main__":
    unittest.main(verbosity=4)
